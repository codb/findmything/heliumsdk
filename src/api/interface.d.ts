export type DataCreditBalance = {
  'dc_balance': number;
  'id': string;
  'name': string;
};

export type Device = {
  'active': boolean;
  'adr_allowed': any;
  'app_eui': string;
  'app_key': string;
  'dev_eui': string;
  'cf_list_enabled': any;
  'config_profile_id': any;
  'dc_usage': number;
  'id': string;
  'in_xor_filter': boolean;
  'labels': Label[];
  'last_connected': string;
  'name': string;
  'organization_id': string;
  'oui': number;
  'rx_delay': number;
  'total_packets': number;
};

export type Event = {
  'category': string;
  'data': {
    'dc': {
      'balance': number;
      'nonce': number;
      'used': number;
    },
    'devaddr': string;
    'fcnt': number;
    'hold_time': number;
    'hotspot': {
      'channel': number;
      'frequency': number;
      'id': string;
      'lat': number;
      'long': number;
      'name': string;
      'rssi': number;
      'snr': number;
      'spreading': string;
    },
    'mac': any,
    'payload': string;
    'payload_size': number;
    'port': number;
  },
  'description': string;
  'device_id': string;
  'frame_down': any;
  'frame_up': number;
  'organization_id': string;
  'reported_at': string;
  'router_uuid': string;
  'sub_category': string;
};

export type Label = {
  id: string;
  name: string;
};

export interface HeliumConsole {
  getDataCreditBalance: () => Promise<DataCreditBalance[]>
  getDevicesList: () => Promise<Device[]>
  getDeviceByUuid: (deviceId: string)=> Promise<Device>
  getDeviceEvents: (deviceId: string) => Promise<Event[]>
  getLabels: () => Promise<Label[]>
}
