import {
  DataCreditBalance, Device, HeliumConsole, Event, Label,
} from './interface';

export class HeliumSdk implements HeliumConsole {
  private baseUrl = 'https://console.helium.com/api/v1/';

  private readonly apiKey;

  constructor(apiKey: string) {
    this.apiKey = apiKey;
  }

  private async get<T>(endpoint: string): Promise<T> {
    const res = await fetch(`${this.baseUrl}${endpoint}`, {
      headers: {
        key: this.apiKey,
      },
    });
    const json = await res.json();
    return json;
  }

  getDataCreditBalance() {
    return this.get<DataCreditBalance[]>('organization');
  }

  getDevicesList() {
    return this.get<Device[]>('devices');
  }

  getDeviceByUuid(deviceId: string) {
    return this.get<Device>(`devices/${deviceId}`);
  }

  getDeviceEvents(deviceId: string) {
    return this.get<Event[]>(`devices/${deviceId}/events`);
  }

  getLabels() {
    return this.get<Label[]>('labels');
  }
}
