module.exports = {
  "env": {
    "es2021": true,
    "node": true
  },
  "extends": [
    "airbnb-base",
    "airbnb-typescript/base"
  ],
  "overrides": [
  ],
  "parserOptions": {
    "ecmaVersion": "latest",
    "sourceType": "module",
    "project": "./tsconfig.json"
  },
  ignorePatterns: ['.eslintrc.js', 'test/*'],
  "rules": {
    'import/prefer-default-export': 0
  }
}
